<?xml version="1.0" encoding="UTF-8"?>
<!-- edited with XMLSPY v2004 rel. 3 U (http://www.xmlspy.com) by Mohana Ramaratnam (Washington University) -->
<Pipeline xmlns="http://nrg.wustl.edu/pipeline" xmlns:ext="org.nrg.validate.utils.ProvenanceUtils" xmlns:xi="http://www.w3.org/2001/XInclude"
         xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://nrg.wustl.edu/pipeline ..\..\schema\pipeline.xsd" 
	 xmlns:fileUtils="http://www.xnat.org/java/org.nrg.imagingtools.utils.FileUtils"
	 xmlns:system="http://www.xnat.org/java/java.lang.System">
	<name>ReleaseXML</name>
	<!--Should be  Name of the pipeline XML file -->
	<location>ReleaseXML</location>
	<!-- Filesystem path to the pipeline XML -->
	<description>Builds combined 3T sessions and transfers session, behavioral and restricted data from Intradb to ConnectomeDB</description>
	<resourceRequirements>
           <!-- NOTE:  Queues defined in /nrgpackages/sge_root/nrg/common/qtask.  Dev machines have overlay at /opt/sge_common/[DEV_MACH_NAME]/qtask which is mounted as a bind  -->
           <property name="DRMAA_JobTemplate_JobCategory">hcp_priority_q</property>
	</resourceRequirements> 
	<documentation>
	   <authors>
	   	<author>
	   		<lastname>Mike</lastname>
			<firstname>Hodge</firstname>
	   	</author>
	   </authors>
		<version>1</version>
		<input-parameters>
			<parameter>
				<name>project</name>
				<values><schemalink>xnat:subjectData.project</schemalink></values>
				<description>Project ID</description>
			</parameter>
			<parameter>
				<name>subject</name>
				<values><schemalink>xnat:subjectData.label</schemalink></values>
				<description>Subject Label</description>
			</parameter>
			<parameter>
				<name>report-email-list</name>
				<values><csv>DEFAULT</csv></values>
				<description>Comma-separated list of e-mail addresses or file containing them</description>
			</parameter>
			<parameter>
				<name>error-override</name>
				<values>
					<csv>0</csv>
				</values>
				<description>Set the value to 1 if you want the pipeline to override release rules errors (Useful in rare circumstances)</description>
			</parameter>
			<parameter>
				<name>notify</name>
				<values>
					<csv>1</csv>
				</values>
				<description>Set the value to 1 if you want the pipeline to notify the user when complete. 0 otherwise</description>
			</parameter>
		</input-parameters>
	</documentation>
        <xnatInfo appliesTo="xnat:subjectData"/>
      	<outputFileNamePrefix>^concat(/Pipeline/parameters/parameter[name='workdir']/values/unique/text(),'/ReleaseHCP')^</outputFileNamePrefix>
	<!-- Description of the Pipeline -->
	<parameters>
		<parameter>
			<name>workdir</name>
			<values>
				<unique>^concat(/Pipeline/parameters/parameter[name='builddir']/values/unique/text(),'/',/Pipeline/parameters/parameter[name='subject']/values/unique/text())^</unique>
			</values>
		</parameter>
		<parameter>
			<name>config</name>
			<values>
				<unique>/data/intradb/pipeline/catalog/ReleaseHCP/resources/config/doRelease.cfg</unique>
			</values>
		</parameter>
	</parameters>
	<steps>
		<step id="1" description="Update sanity checks" workdirectory="^/Pipeline/parameters/parameter[name='workdir']/values/unique/text()^">
			<resource name="callSanityChecks" location="SanityChecks/resources" >
				<argument id="host">
					<value>^/Pipeline/parameters/parameter[name='host']/values/unique/text()^</value>
				</argument>
				<argument id="user">
					<value>^/Pipeline/parameters/parameter[name='user']/values/unique/text()^</value>
				</argument>
				<argument id="pw">
					<value>^/Pipeline/parameters/parameter[name='pwd']/values/unique/text()^</value>
				</argument>
				<argument id="buildDir">
					<value>^/Pipeline/parameters/parameter[name='workdir']/values/unique/text()^</value>
				</argument>
				<argument id="project">
					<value>^/Pipeline/parameters/parameter[name='project']/values/unique/text()^</value>
				</argument>
				<argument id="subject">
					<value>^/Pipeline/parameters/parameter[name='subject']/values/unique/text()^</value>
				</argument>
				<argument id="create-report"/>
				<argument id="send-report">
					<value>^/Pipeline/parameters/parameter[name='report-email-list']/values/unique/text()^</value>
				</argument>
				<argument id="verbose"/>
			</resource>
		</step>
		<step id="2" description="Verify that all sanity checks have been run and passed or accepted" workdirectory="^/Pipeline/parameters/parameter[name='workdir']/values/unique/text()^">
			<resource name="checkSanityCheckStatus" location="ReleaseHCP/resources" >
				<argument id="host">
					<value>^/Pipeline/parameters/parameter[name='host']/values/unique/text()^</value>
				</argument>
				<argument id="user">
					<value>^/Pipeline/parameters/parameter[name='user']/values/unique/text()^</value>
				</argument>
				<argument id="pw">
					<value>^/Pipeline/parameters/parameter[name='pwd']/values/unique/text()^</value>
				</argument>
				<argument id="project">
					<value>^/Pipeline/parameters/parameter[name='project']/values/unique/text()^</value>
				</argument>
				<argument id="subject">
					<value>^/Pipeline/parameters/parameter[name='subject']/values/unique/text()^</value>
				</argument>
				<argument id="errorOnFail"/>
			</resource>
		</step>
		<step id="3a" description="Create and transfer combined session" workdirectory="^/Pipeline/parameters/parameter[name='workdir']/values/unique/text()^" precondition="^/Pipeline/parameters/parameter[name='error-override']/values/unique/text()!='1'^">
			<resource name="doRelease" location="ReleaseHCP/resources" >
				<argument id="sourceServer">
					<value>^/Pipeline/parameters/parameter[name='host']/values/unique/text()^</value>
				</argument>
				<argument id="sourceUser">
					<value>^/Pipeline/parameters/parameter[name='user']/values/unique/text()^</value>
				</argument>
				<argument id="sourcePass">
					<value>^/Pipeline/parameters/parameter[name='pwd']/values/unique/text()^</value>
				</argument>
				<argument id="sourceProj">
					<value>^/Pipeline/parameters/parameter[name='project']/values/unique/text()^</value>
				</argument>
				<argument id="subjectList">
					<value>^/Pipeline/parameters/parameter[name='subject']/values/unique/text()^</value>
				</argument>
				<argument id="config">
					<value>^/Pipeline/parameters/parameter[name='config']/values/unique/text()^</value>
				</argument>
			</resource>
		</step>
		<step id="3b" description="Create and transfer combined session" workdirectory="^/Pipeline/parameters/parameter[name='workdir']/values/unique/text()^" precondition="^/Pipeline/parameters/parameter[name='error-override']/values/unique/text()='1'^">
			<resource name="doRelease" location="ReleaseHCP/resources" >
				<argument id="sourceServer">
					<value>^/Pipeline/parameters/parameter[name='host']/values/unique/text()^</value>
				</argument>
				<argument id="sourceUser">
					<value>^/Pipeline/parameters/parameter[name='user']/values/unique/text()^</value>
				</argument>
				<argument id="sourcePass">
					<value>^/Pipeline/parameters/parameter[name='pwd']/values/unique/text()^</value>
				</argument>
				<argument id="sourceProj">
					<value>^/Pipeline/parameters/parameter[name='project']/values/unique/text()^</value>
				</argument>
				<argument id="subjectList">
					<value>^/Pipeline/parameters/parameter[name='subject']/values/unique/text()^</value>
				</argument>
				<argument id="config">
					<value>^/Pipeline/parameters/parameter[name='config']/values/unique/text()^</value>
				</argument>
				<argument id="errorOverride"/>
			</resource>
		</step>
		<step id="END-Notify" description="Notify" precondition="^/Pipeline/parameters/parameter[name='notify']/values/unique/text()!='0'^">
			<resource name="Notifier" location="notifications">
                                <argument id="user">
                                        <value>^/Pipeline/parameters/parameter[name='user']/values/unique/text()^</value>
                                </argument>
                                <argument id="password">
                                        <value>^/Pipeline/parameters/parameter[name='pwd']/values/unique/text()^</value>
                                </argument>
				<argument id="to">
					<value>^/Pipeline/parameters/parameter[name='useremail']/values/unique/text()^</value>
				</argument>
				<argument id="cc">
					<value>^/Pipeline/parameters/parameter[name='adminemail']/values/unique/text()^</value>
				</argument>
				<argument id="from">
					<value>^/Pipeline/parameters/parameter[name='adminemail']/values/unique/text()^</value>
				</argument>
				<argument id="subject">
					<value>^concat(/Pipeline/parameters/parameter[name='xnatserver']/values/unique/text(), ' update: ConnectomeDB session generated for ',/Pipeline/parameters/parameter[name='subject']/values/unique/text() )^</value>
				</argument>
				<argument id="host">
					<value>^/Pipeline/parameters/parameter[name='mailhost']/values/unique/text()^</value>
				</argument>
				<argument id="body">
					<value>^concat('Dear ',/Pipeline/parameters/parameter[name='userfullname']/values/unique/text(),',&lt;br&gt; &lt;p&gt;', ' A ConnectomeDB session has been generated for subject ', /Pipeline/parameters/parameter[name='subject']/values/unique/text(),' . Details of the  session are available  &lt;a href="',/Pipeline/parameters/parameter[name='host']/values/unique/text(),'/app/action/DisplayItemAction/search_element/xnat:mrSessionData/search_field/xnat:mrSessionData.ID/search_value/',/Pipeline/parameters/parameter[name='xnat_id']/values/unique/text(),'"&gt;', ' here. &lt;/a&gt; &lt;/p&gt;&lt;br&gt;', ' &lt;/p&gt;&lt;br&gt;', 'XNAT Team.')^
					</value>
				</argument>
			</resource>
		</step>
	</steps>
</Pipeline>
