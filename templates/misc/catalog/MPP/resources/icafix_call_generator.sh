#!/bin/bash -x
set -e

#This script will generate a job file for ICA+FIX analysis 

#DB ICA+FIX Analysis Processing pipeline Job File Generator
#Author: Mohana Ramaratnam (mohanakannan9@gmail.com)
#Version 0.1 Date: November 15, 2013

#Inputs: 
# path of param file
# path to location where the job file would be generated

#The PBS/SGE statements are picked from a config file for the specific cluster

isSet() {
   if [[ ! ${!1} && ${!1-_} ]] ; then
        echo "$1 is not set, aborting."
        exit 1
   elif [ -z ${!1} ] ; then	
        echo "$1 has no value, aborting."
        exit 1
   fi
}


ARGS=4
program="$0"

if [ $# -ne "$ARGS" ]
then
  echo "Usage: `basename $program` Path_to_paramsFile XNAT_Password Path_to_outFile"
  exit 1
fi


paramsFile=$1
passwd=$2
outFile=$3
dppParamsFile=$4


dirname=`dirname "$program"`
configdir="${dirname}/config"


source $paramsFile

source $dppParamsFile


#Default to CHPC
if [ X$compute_cluster = X ] ; then
  $compute_cluster = CHPC
fi

if [ $compute_cluster = CHPC ] ; then
   configurationForJobFile=$configdir/CHPC/rfMRIFIXHCP.pbs.config  
   processingParamsFile=$configdir/CHPC/rfMRIFIXHCP.pbs.param   	
elif [ X$compute_cluster = NRG ] ; then
   configurationForJobFile=$configdir/CHPC/rfMRIFIXHCP.sge.config
   processingParamsFile=$configdir/CHPC/rfMRIFIXHCP.sge.param   	
fi

if [ ! -f $configurationForJobFile ] ; then
  echo "File at $configurationForJobFile doesnt exist. Aborting!"
  exit 1;
fi

if [ ! -f $processingParamsFile ] ; then
  echo "File at $processingParamsFile doesnt exist. Aborting!"
  exit 1;
fi

#The processing params file would contain path to the configdir, CaretAtlasDir, templatesdir
source $processingParamsFile

###########################################################
# Check if the variables expected are defined
#
###########################################################

isSet icafix_bp
isSet icafix_functseries

###########################################################
# Continue - looks good
#
###########################################################



touch $outFile

if [ ! -f $outFile ] ; then
  echo "File at $outFile doesnt exist. Aborting!"
  exit 1;
fi

cat $configurationForJobFile > $outFile


commandStr="$PIPELINE_HOME/bin/XnatPipelineLauncher -pipeline rfMRIFIXHCP/rfMRIFIXHCP.xml -project $project -id $xnat_id -dataType $dataType -host $xnat_host -parameter xnatserver=$xnatserver -parameter project=$project -parameter xnat_id=$xnat_id -label $label -u $user -pwd $passwd -supressNotification -notify $userEmail -notify $adminEmail -parameter adminemail=$adminEmail -parameter useremail=$userEmail -parameter mailhost=$mailhost -parameter userfullname=$userfullname -parameter builddir=$builddir -parameter sessionid=$sessionId -parameter subjects=$subject  -parameter templatesdir=$templatesdir  -parameter configdir=$configdir -parameter CaretAtlasDir=$CaretAtlasDir -parameter BP=$icafix_bp -parameter functseries=$icafix_functseries -parameter compute_cluster=$compute_cluster -parameter packaging_outdir=$packaging_outdir  -parameter cluster_builddir_prefix=$cluster_builddir_prefix -parameter db_builddir_prefix=$db_builddir_prefix "


echo "Creating $outFile" 

echo "echo \" \"" >> $outFile
echo "$commandStr" >> $outFile
echo "echo \" \"" >> $outFile
echo "echo \"Job finished  at \`date\`\"" >> $outFile


echo "exit 0;" >> $outFile

chmod +x $outFile

exit 0

