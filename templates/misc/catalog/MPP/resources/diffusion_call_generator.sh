#!/bin/bash -x
set -e

#This script will generate a job file for DiffusionHCP processing

#DB Diffusion Processing pipeline Job File Generator
#Author: Mohana Ramaratnam (mohanakannan9@gmail.com)
#Version 0.1 Date: November 15, 2013

#Inputs: 
# path of param file
# path to location where the job file would be generated

#The PBS/SGE statements are picked from a config file for the specific cluster

isSet() {
   if [[ ! ${!1} && ${!1-_} ]] ; then
        echo "$1 is not set, aborting."
        exit 1
   elif [ -z ${!1} ] ; then	
        echo "$1 has no value, aborting."
        exit 1
   fi
}


ARGS=6
program="$0"

if [ $# -ne "$ARGS" ]
then
  echo "Usage: `basename $program` Path_to_paramsFile Path_to_MppPipelineParamsFile XNAT_Password Path_to_outFile MPP_PARAM_FILE_PATH"
  exit 1
fi


paramsFile=$1
mpp_pipelineparamsFile=$2
passwd=$3
outFile=$4
mppParamsFile=$5
jsession=$6

dirname=`dirname "$program"`
configdir="${dirname}/config"


source $paramsFile
source $mpp_pipelineparamsFile

source $mppParamsFile

PIPELINE_NAME="DiffusionHCP/DiffusionHCP.xml"


#Default to CHPC
if [ X$compute_cluster = X ] ; then
  compute_cluster=CHPC
fi

if [ $compute_cluster = CHPC ] ; then
   configurationForJobFile=$configdir/CHPC/diffusionHCP.pbs.config  
   processingParamsFile=$configdir/CHPC/diffusionHCP.pbs.param   	
elif [ X$compute_cluster = NRG ] ; then
   configurationForJobFile=$configdir/CHPC/diffusionHCP.sge.config
   processingParamsFile=$configdir/CHPC/diffusionHCP.sge.param   	
fi

if [ ! -f $configurationForJobFile ] ; then
  echo "File at $configurationForJobFile doesnt exist. Aborting!"
  exit 1;
fi

if [ ! -f $processingParamsFile ] ; then
  echo "File at $processingParamsFile doesnt exist. Aborting!"
  exit 1;
fi

#The processing params file would contain path to the configdir, CaretAtlasDir, templatesdir
source $processingParamsFile

###########################################################
# Check if the variables expected are defined
#
###########################################################

isSet diffusion_EchoSpacing
isSet diffusion_PhaseEncodingDir
isSet diffusion_DiffusionDirDictNames[0]
isSet diffusion_DiffusionDirDictValues[0]
isSet diffusion_DiffusionScanDictValues[0]
isSet diffusion_DiffusionScanDictNames[0]

if [[ ${#diffusion_DiffusionDirDictNames[@]} -ne ${#diffusion_DiffusionDirDictValues[@]} || ${#diffusion_DiffusionDirDictNames[@]} -ne ${#diffusion_DiffusionScanDictValues[@]} || ${#diffusion_DiffusionDirDictNames[@]} -ne ${#diffusion_DiffusionScanDictNames[@]}  ]]; then 
  echo " The array lengths of variables diffusion_DiffusionDirDictNames, diffusion_DiffusionDirDictValues, diffusion_DiffusionScanDictValues, diffusion_DiffusionScanDictNames dont match. Aborting! " 
  exit 1
fi 

###########################################################
# Continue - looks good
#
###########################################################



touch $outFile

if [ ! -f $outFile ] ; then
  echo "File at $outFile doesnt exist. Aborting!"
  exit 1;
fi

cat $configurationForJobFile > $outFile


workflowID=`source $SCRIPTS_HOME/epd-python_setup.sh; python $PIPELINE_HOME/catalog/ToolsHCP/resources/scripts/workflow.py -User $user -Password $passwd -Server $host -ExperimentID $xnat_id -ProjectID $project -Pipeline $PIPELINE_NAME -Status Queued -JSESSION $jsession`
if [ $? -ne 0 ] ; then
	echo "Fetching workflow for structural failed. Aborting!"
	exit 1
fi 

commandStr="$PIPELINE_HOME/bin/XnatPipelineLauncher -pipeline $PIPELINE_NAME -project $project -id $xnat_id -dataType $dataType -host $xnat_host -parameter xnatserver=$xnatserver -parameter project=$project -parameter xnat_id=$xnat_id -label $label -u $user -pwd $passwd -supressNotification -notify $useremail -notify $adminemail -parameter adminemail=$adminemail -parameter useremail=$useremail -parameter mailhost=$mailhost -parameter userfullname=$userfullname -parameter builddir=$builddir -parameter sessionid=$sessionId -parameter subjects=$subject  -parameter templatesdir=$templatesdir  -parameter configdir=$configdir -parameter CaretAtlasDir=$CaretAtlasDir -parameter EchoSpacing=$diffusion_EchoSpacing -parameter PhaseEncodingDir=$diffusion_PhaseEncodingDir -parameter compute_cluster=$compute_cluster -parameter packaging_outdir=$packaging_outdir  -parameter cluster_builddir_prefix=$cluster_builddir_prefix -parameter db_builddir_prefix=$db_builddir_prefix -workFlowPrimaryKey $workflowID"


#Now for the 95, 96, 97 Direction scans append the parameters to the command

index=0
for diffParameter in "${diffusion_DiffusionDirDictNames[@]}"
do
	diffDirParameterValue=${diffusion_DiffusionDirDictValues[$index]}
	diffParameterValue=${diffusion_DiffusionScanDictValues[$index]}
	diffParameterName=${diffusion_DiffusionScanDictNames[$index]}
	
	commandStr=" $commandStr -parameter ${diffParameter}=${diffDirParameterValue} -parameter ${diffParameterName}=${diffParameterValue} "
	index=$((index+1))
done

echo "Creating $outFile" 

echo "echo \" \"" >> $outFile
echo "$commandStr" >> $outFile
echo "rc_command=\$?" >> $outFile

echo "echo \$rc_command \" \"" >> $outFile
echo "echo \"Job finished  at \`date\`\"" >> $outFile


echo "exit \$rc_command" >> $outFile

chmod +x $outFile

exit 0

