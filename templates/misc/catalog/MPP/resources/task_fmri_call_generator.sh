#!/bin/bash -x
set -e

#This script will generate a job file for TaskfMRIHCP processing

#DB Structural Processing pipeline Job File Generator
#Author: Mohana Ramaratnam (mohanakannan9@gmail.com)
#Version 0.1 Date: November 15, 2013

#Inputs: 
# path of param file
# path to location where the job file would be generated

#The PBS/SGE statements are picked from a config file for the specific cluster


isSet() {
   if [[ ! ${!1} && ${!1-_} ]] ; then
        echo "$1 is not set, aborting."
        exit 1
   elif [ -z ${!1} ] ; then	
        echo "$1 has no value, aborting."
        exit 1
   fi
}

ARGS=4
program="$0"

if [ $# -ne "$ARGS" ]
then
  echo "Usage: `basename $program` Path_to_paramsFile XNAT_Password Path_to_outDir"
  exit 1
fi


paramsFile=$1
passwd=$2
outDir=$3
dppParamsFile=$4

source $paramsFile
source $dppParamsFile


dirname=`dirname "$program"`
configdir="${dirname}/config"


#Default to CHPC
if [ X$compute_cluster = X ] ; then
  $compute_cluster = CHPC
fi

if [ $compute_cluster = CHPC ] ; then
   configurationForJobFile=$configdir/CHPC/taskfMRIHCP.pbs.config  
   processingParamsFile=$configdir/CHPC/taskfMRIHCP.pbs.param   	
fi

if [ X$compute_cluster = NRG ] ; then
   configurationForJobFile=$configdir/NRG/taskfMRIHCP.sge.config
   processingParamsFile=$configdir/NRG/taskfMRIHCP.sge.param   	
fi

if [ ! -f $configurationForJobFile ] ; then
  echo "File at $configurationForJobFile doesnt exist. Aborting!"
  exit 1;
fi

if [ ! -f $processingParamsFile ] ; then
  echo "File at $processingParamsFile doesnt exist. Aborting!"
  exit 1;
fi

#The processing params file would contain path to the configdir, CaretAtlasDir, templatesdir
source $processingParamsFile

###########################################################
# Check if the variables expected are defined
#
###########################################################

isSet taskfMRI_lowresmesh
isSet taskfMRI_origsmoothingfwhm
isSet taskfMRI_finalsmoothingfwhm
isSet taskfMRI_grayordinates
isSet taskfMRI_functroot[0]
isSet taskfMRI_functseries[0]
isSet taskfMRI_confound
isSet taskfMRI_vba

if [[ ${#taskfMRI_functroot[@]} -ne ${#taskfMRI_functseries[@]} ]] ; then
  echo "The array lengths of variables taskfMRI_functroot, taskfMRI_functseries do not match. Aborting!" 
  exit 1
fi

###########################################################
# Continue - looks good
#
###########################################################


index=0
for tscan in "${taskfMRI_functroot[@]}"
do

  #For each scan create the outfile of command to launch the Functional processing pipeline

	tseries=${taskfMRI_functseries[$index]}
	outFile=${outDir}/${subject}_${tscan}_taskanalysis.sh
	touch $outFile

	if [ ! -f $outFile ] ; then
	  echo "File at $outFile doesnt exist. Aborting!"
	  exit 1;
	fi
	

	cat $configurationForJobFile > $outFile
	commandStr="$PIPELINE_HOME/bin/XnatPipelineLauncher -pipeline TaskfMRIHCP/TaskfMRIHCP.xml -project $project -id $xnat_id -dataType $dataType -host $xnat_host -parameter xnatserver=$xnatserver -parameter project=$project -parameter xnat_id=$xnat_id -label $label -u $user -pwd $passwd -supressNotification -notify $userEmail -notify $adminEmail -parameter adminemail=$adminEmail -parameter useremail=$userEmail -parameter mailhost=$mailhost -parameter userfullname=$userfullname -parameter builddir=$builddir -parameter sessionid=$sessionId -parameter subjects=$subject -parameter functroot=$tscan -parameter functseries=$tseries -parameter lowresmesh=$taskfMRI_lowresmesh -parameter grayordinates=$taskfMRI_grayordinates -parameter origsmoothingFWHM=$taskfMRI_origsmoothingfwhm -parameter finalsmoothingFWHM=$taskfMRI_finalsmoothingfwhm -parameter temporalfilter=$taskfMRI_temporalfilter -parameter confound=$taskfMRI_confound -parameter vba=$taskfMRI_vba -parameter templatesdir=$templatesdir  -parameter configdir=$configdir -parameter CaretAtlasDir=$CaretAtlasDir -parameter compute_cluster=$compute_cluster -parameter packaging_outdir=$packaging_outdir -parameter cluster_builddir_prefix=$cluster_builddir_prefix -parameter db_builddir_prefix=$db_builddir_prefix  "

	echo "Creating $outFile" 
	echo "echo \" \"" >> $outFile

	echo "$commandStr" >> $outFile
	echo "echo \" \"" >> $outFile

	echo "echo \"Job finished  at \`date\`\"" >> $outFile

	echo "exit 0;" >> $outFile
	
	chmod +x $outFile

	index=$((index+1))

done

exit 0