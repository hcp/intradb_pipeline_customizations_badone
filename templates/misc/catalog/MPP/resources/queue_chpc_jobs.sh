#!/bin/bash 


#This script will submit jobs to the PBS managed CHPC cluster for a subject taking care of the job dependencies

#DB Data Processing pipeline Job submittor
#Author: Mohana Ramaratnam (mohanakannan9@gmail.com)
#Version 0.1 Date: November 15, 2013

#Inputs: 
# path of param file


############################################################################
#
# Convenience Methods
#
############################################################################

get_jobid() {
  #$1: Series Description of the scan
  index=0
  found=0
  for fseries in ${functional_functionalseries[@]}
  do
    if [ $fseries = $1 ]; then
      found=1
      break
    fi
    index=$((index+1))
  done
  
  
  #Did we find the series description
  if [ $found -eq 0 ]; then
    echo $found
  else
   echo ${functionalJobIds[$index]}  
  fi
  
}


isQueuedFSF() {
  #$1: Series Description of the scan
  isQueued=0
  for fseries in "${queued[@]}"
  do
    if [ $fseries = $1 ] ; then
      isQueued=1
      break
    fi
  done
  echo $isQueued
}

paramsFile=$1
passwd=$2
path_to_scripts=$3

declare -a functionalJobIds

declare -a queued

functionalJobIds=()
queued=()

source $paramsFile

############################################################################
#
#Submit the structural job
#
############################################################################

queueLogFile=$path_to_scripts/queue.log

if [ -e $queueLogFile ] ; then
   \rm $queueLogFile
fi

touch $queueLogFile





if [ $launchStructural -eq 1 ] ; then	
     STRUCTURAL_START_JOB_ID=`qsub -V $path_to_scripts/${subject}_structural.sh`
     if [ $? -ne 0 ] ; then
        echo "Submission of  $path_to_scripts/${subject}_structural.sh failed. Aborting!"
        exit 1
     else 
        dependencyControl=" -W depend=afterok:${STRUCTURAL_START_JOB_ID} "
 	STRUCTURAL_JOB_ID=`qsub -V $dependencyControl $path_to_scripts/${subject}_structural_end.sh`
 	echo "Structural Processing job submitted for subject $subject. JOB ID = $STRUCTURAL_START_JOB_ID"
 	echo "Structural PUT job submitted for subject $subject. JOB ID = $STRUCTURAL_JOB_ID"
 	echo "Structural Processing job submitted for subject $subject. JOB ID = $STRUCTURAL_START_JOB_ID" >> $queueLogFile
 	echo "Structural PUT job submitted for subject $subject. JOB ID = $STRUCTURAL_JOB_ID" >> $queueLogFile
     fi
fi

############################################################################
#
#Now submit the functional jobs - these cannt be run unless the Structural are done
#
############################################################################

if [ $launchFunctional -eq 1 ] ; then
     index=0
     for fscan in "${functional_scanid[@]}"
     do
	fseries=${functional_functionalseries[$index]}
	fseriesRoot=`echo $fseries | awk '{gsub(/_LR/,""); gsub(/_RL/,""); print}'`

	dependencyControl=" "
	if [ $launchStructural -eq 1 ] ; then
           dependencyControl=" -W depend=afterok:${STRUCTURAL_JOB_ID} "
	fi
	FUNCTIONAL_JOB_ID=`qsub -V $dependencyControl $path_to_scripts/${subject}_${fseries}_functional.sh`
	if [ $? -ne 0 ] ; then
	  echo "Submission of  $path_to_scripts/${subject}_${fseries}_functional.sh failed. Aborting!"
	  exit 1
	else 
	   echo "Functional job for $subject scan $fscan has been queued. JOB ID = $FUNCTIONAL_JOB_ID"
	   echo "Functional job for $subject scan $fscan has been queued. JOB ID = $FUNCTIONAL_JOB_ID" >> $queueLogFile
	fi
	functionalJobIds[$index]=$FUNCTIONAL_JOB_ID
	let index=index+1;
#	index=$((index+1))
      done
fi



############################################################################
#
#Now submit the FSF creation job - these cannt be run unless the Functionals are done
#
############################################################################


if [ $launchFunctional -eq 1 ] ; then

     index=0
     j=0
     for fscan in "${functional_scanid[@]}"
     do
	fseries=${functional_functionalseries[$index]}
	fseriesRoot=`echo $fseries | awk '{gsub(/_LR/,""); gsub(/_RL/,""); print}'`
	echo ${queued[@]}

	isQueuedFSF=`isQueuedFSF $fseriesRoot`
	if [ $isQueuedFSF -eq 0 ]; then 
		lr_task_series=${fseriesRoot}_LR
		rl_task_series=${fseriesRoot}_RL
		lr_job_id=`get_jobid $lr_task_series`
		rl_job_id=`get_jobid $rl_task_series`

		dependencyControl=" -W depend=afterok:${lr_job_id}:${rl_job_id} "
		FUNCTIONAL_JOB_ID=`qsub -V $dependencyControl $path_to_scripts/${subject}_${fseriesRoot}_end.sh`
		if [ $? -ne 0 ] ; then
		  echo "Submission of  $path_to_scripts/${subject}_${fseriesRoot}_fsf.sh failed. Aborting!"
		  exit 1
		else 
		   echo "FSF job for $subject scan $fseriesRoot has been queued. JOB ID = $FUNCTIONAL_JOB_ID"
		   echo "FSF job for $subject scan $fseriesRoot has been queued. JOB ID = $FUNCTIONAL_JOB_ID" >> $queueLogFile
		fi
		queued[$j]=$fseriesRoot
		j=$((j+1))
		echo ${queued[@]}
	fi
	index=$((index+1))
      done
fi


############################################################################
#
#Now submit the diffusion job - depends on structural 
#
############################################################################

if [ $launchDiffusion -eq 1 ] ; then
	dependencyControl=" "
	if [ $launchStructural -eq 1 ] ; then
	   dependencyControl=" -W depend=afterok:$STRUCTURAL_JOB_ID " 	
	fi
	DIFFUSION=`qsub -V $dependencyControl $path_to_scripts/${subject}_diffusion.sh`
	if [ $? -ne 0 ] ; then
	  echo "Submission of  $path_to_scripts/${subject}_diffusion.sh failed. Aborting!"
	  exit 1
	else 
	  echo "Diffusion job for $subject has been queued. JOB ID = $DIFFUSION"
	  echo "Diffusion job for $subject has been queued. JOB ID = $DIFFUSION" >> $queueLogFile
	fi
fi

############################################################################
#
#Now submit the Task-fMRI Analysis pipeline
#
############################################################################

#These cannt be run unless the corresponding Functional is done - and hence Structural 
#if [ $launchTask -eq 1 ] ; then
#	index=0
#	for tscan in "${taskfMRI_functroot[@]}"
#	do
#	dependencyControl=" "
#	if [ $launchFunctional -eq 1 ] ; then
#	  lr_task_series=${tscan}_LR
#	  rl_task_series=${tscan}_RL
#	  lr_job_id=`get_jobid $lr_task_series`
#	  rl_job_id=`get_jobid $rl_task_series`
#	  if [ [ $lr_job_id -ne 1 ] && [ $rl_job_id -ne 1 ] ] ; then
#	    dependencyControl=" -W depend=afterok:${lr_job_id}:${rl_job_id} "	
#	  fi 
#	fi  
#	  #Now submit the task job making it dependent on the completion of the two functional jobs
#	  TASK_JOB_ID=`qsub -V $dependencyControl $path_to_scripts/${subject}_${tscan}_taskanalysis.sh`  
#	  if [ $? -ne 0 ] ; then
#	    echo "Submission of  $path_to_scripts/${subject}_${tscan}_taskanalysis.sh failed. Aborting!"
#	    exit 1
#	  else 
#	    echo "Task Analysis job for $subject task $tscan has been queued. It depends on completion of jobs = ${lr_job_id}, ${rl_job_id}"
#	  fi
#	done
#	
#fi

############################################################################
#
#Now submit the ICA+FIX Analysis pipeline 
#
############################################################################


exit 0