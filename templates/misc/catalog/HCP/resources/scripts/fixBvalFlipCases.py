#! /usr/bin/env python
__author__ = "mhodge01,mhouse01"

import requests
import base64
import mmap
import re
import sys
import urllib
import urllib2
import json
import shutil
import os
from datetime import datetime
from lxml import etree
import xml.dom.minidom as minidom
from sys import exit
from operator import attrgetter
import argparse
import ConfigParser
import tempfile

# Declare a Namespace map for use in XML parsing
XNAT_NS = 'http://nrg.wustl.edu/xnat' 
CAT_NS = 'http://nrg.wustl.edu/catalog'  
NSMAP = { 'xnat' : XNAT_NS , 'cat' : CAT_NS } 
CATMAP = { 'cat' : 'http://nrg.wustl.edu/catalog' } 

rawExt = '_unproc'

xmlFormat =  {'format': 'xml'}
jsonFormat = {'format': 'json'}
qualityDict = dict(unusable=0, poor=1, fair=2, good=3, excellent=4, usable=5, undetermined=6)
## value will be set for each subject in method called in main processing section
subjLabel = None

global idet        
global overwrite
global verbose
## How many times to try HTTP requests
MAXTRYS = 3
## Initial timeout
TIMEOUT = 300

class SeriesDetails:
    """A simple class to store information about a scan series"""
    def __init__(self):
        self.seriesNum = None
        self.seriesQualityText = None
        self.seriesQualityNumeric = None
        self.seriesDescOrig = None
        self.seriesDesc = None
        self.niftiCount = None
        self.seriesDate = None
        self.isUnique = None
        self.instanceNum = None
        self.instanceName = None
        self.instanceIncluded = None
        self.fileList = None
    def __repr__(self):
        return "<SeriesDetails seriesNum:%s seriesQualityText:%s seriesQualityNumeric:%s seriesDesc:%s niftiCount:%s seriesDate:%s>" \
               % (self.seriesNum, self.seriesQualityText, self.seriesQualityNumeric, self.seriesDesc, self.niftiCount, self.seriesDate)

class ReleaseAndConnDetails:
    """A simple class to store information about a scan series"""
    def __init__(self):
        self.target = None
        self.expList = None
        self.releaseDesc = None
        self.releaseDate = None
        self.mrLabelExt = None
        self.ntLabelExt = None
    def __repr__(self):
        return "<InputDetails target=%s subjects=%s release=%s>" \
               % (self.target, self.expList, self.releaseDate)

class TargetDetails:
    """A simple class to store information about a scan series"""
    def __init__(self):
        self.server = None
        self.insecure = None
        self.user = None
        self.pw = None
        self.proj = None
        self.urlRoot = None
        self.header = None
    def __repr__(self):
        return "%s:%s:%s" \
               % (self.server, self.user, self.proj)
               
class ScanFileInfo:
    """A simple class to store information about a scan series"""
    def __init__(self):
        self.subject = None
        self.fileType = None
        self.otherInfo = None
        self.srcExptLabel = None
        self.srcFileURI = None
        self.srcSeriesDesc = None
        self.srcFileName = None
        self.destResourceURI = None
        self.destSeriesDesc = None
        self.destScanID = None
    def __repr__(self):
        return "<ScanFileInfo:  source=%s dest=%s>" \
               % (self.srcFileURI, self.desetResourceURI)
               
class FileType:               
    NIFTI=1
    SNAPSHOT_ORIGINAL=2
    SNAPSHOT_THUMBNAIL=3
               
class Target:               
    SOURCE=1
    DEST=2

def parseInputReturnParser():
    parser = argparse.ArgumentParser(description="Alpha program to pull NIFTI data from XNAT and package it for FTP distribution")
    
    parser.add_argument("-c", "--config", dest="configFile", default=None, type=str, help="config file must be specified")
    parser.add_argument("-v", "--verbose", dest="verbose", default=False, action="store_true", help="show more verbose output")
    parser.add_argument("-l", "--expList", dest="expList", help="list of experiments")
    parser.add_argument("-L", "--mrLabelExt", dest="mrLabelExt", help="Label extension for combined MR Session")
    parser.add_argument("-o", "--overwrite", dest="overwrite", default=False, action="store_true", help="Overwrite existing destination site data?")
    parser.add_argument("-F", "--skipFileUpload", dest="skipFileUpload", default=False, action="store_true", help="Skip file upload?")
    
    parser.add_argument("-s", "--server", dest="server", type=str, help="specify which server to connect to")
    parser.add_argument("-i", "--insecure", dest="insecure", default=False, action="store_true", help="specify whether to use security")
    parser.add_argument("-u", "--user", dest="user", type=str, help="username must be specified")
    parser.add_argument("-p", "--password", dest="password", type=str, help="password must be specified")
    parser.add_argument("-j", "--proj", dest="proj", type=str, help="specify project")
    
    parser.add_argument('--version', action='version', version='%(prog)s: v0.1')
    
    return parser

def str2bool(v):
    return v.upper() in ('T','TRUE','1','Y','YES')
    
def treeFindText(eltree,ele,ns):
    findR = eltree.find(ele,ns) if ns else eltree.find(ele)
    if findR is not None:
        return findR.text    
    else:
        return None
               
def getReleaseAndConnDetailsFromInput():

    details = ReleaseAndConnDetails()
    details.target = TargetDetails()
    details.destTarget = TargetDetails()

    args = parseInputReturnParser().parse_args()
    
    config = ConfigParser.ConfigParser()
    if args.configFile != None:
        config.readfp(open(args.configFile))
    else:
        config = None
    
    details.target.server = str(args.server if args.server or config == None else config.get('Host','server')) 
    details.target.insecure = args.insecure if args.insecure or config == None else config.get('Host','insecure') 
    details.target.user = str(args.user if args.user or config == None else config.get('Host','username')) 
    details.target.pw = str(args.password if args.password or config == None else config.get('Host','password')) 
    details.target.proj = str(args.proj if args.proj or config == None else config.get('Host','project')) 
    if details.target.server.startswith("http"):
        details.target.urlRoot = details.target.server 
    else:
        details.target.urlRoot = "http://" + details.target.server + ":8080" if details.target.insecure else  "https://" + details.target.server 
        #details.target.urlRoot = "http://" + details.target.server + ":80" if details.target.insecure else  "https://" + details.target.server 
    setSessionHeader(details.target)
    
    details.expList = str(args.expList if args.expList or config == None else config.get('Subjects','expList')).rsplit(",")
    details.mrLabelExt = str(args.mrLabelExt if args.mrLabelExt or config == None else config.get('ReleaseInfo','mrlabelext'))
    
    global overwrite
    overwrite = args.overwrite
    
    global skipFileUpload
    skipFileUpload = args.skipFileUpload
    
    global verbose
    verbose = args.verbose

    if (verbose):
  
        print "\nINPUT PARAMETERS:\n"
        print "server=" + details.target.server
        print "insecure=" + str(details.target.insecure)
        print "user=" + details.target.user
        print "pass=" + re.sub(".","X",details.target.pw)
        print "proj=" + details.target.proj + '\n'
    
        print "overwrite=" + str(overwrite)
        print "verbose=" + str(verbose)
        print "expList=" + str(details.expList)
        print "mrLabelExt=" + details.mrLabelExt + '\n'
    
        print  "BEGIN PROCESSING SUBJECTS:\n"
       
    return details       

def setSessionHeader(target):

#    # If we find an OS certificate bundle, use it instead of the built-in bundle
#    if requests.utils.get_os_ca_bundle_path() and not target.insecure:
#        os.environ['REQUESTS_CA_BUNDLE'] = requests.utils.get_os_ca_bundle_path()
#        print "Using CA Bundle: %s" % requests.utils.DEFAULT_CA_BUNDLE_PATH

    # Establish a Session ID
    try:
        r = requests.get( target.urlRoot + "/data/JSESSION", auth=(target.user, target.pw), verify=False )
        # If we don't get an OK; code: requests.codes.ok
        r.raise_for_status()
    # Check if the REST Request fails
    except (requests.ConnectionError, requests.exceptions.RequestException) as e:
        print "Failed to retrieve REST Session ID (%s):" % target.urlRoot
        print "    " + str( e )
        exit(1)

    global restSessionID
    restSessionID = r.content
    print "Rest Session ID (%s): %s" % (target.urlRoot,restSessionID)
    target.header = {"Cookie": "JSESSIONID=" + restSessionID}
    
    
def filePostResponse(targetD,url,files,raise_for_status=True):
    tov = TIMEOUT
    for trycount in range(1,MAXTRYS):
        try:
            r = requests.post(targetD.urlRoot + url,files=files, params=xmlFormat, headers=targetD.header, timeout=tov, verify=False )
            # If we don't get an OK; code: requests.codes.ok
            if (raise_for_status):
                r.raise_for_status()
        # Check if the REST Request fails
        except (requests.Timeout) as e:
            if trycount==MAXTRYS:    
                print "Timed out while attempting to POST FILE - (URL=%s) - TRYING AGAIN:" % (url)
                print "    " + str( e )
                exit(1)
            else:    
                tov += 300
                print "Timed out while attempting to POST FILE - (URL=%s) - TRYING AGAIN:" % (url)
            if not targetD.insecure:
                print "Note that insecure connections are only allowed locally"
        # Check if the REST Request fails
        except (requests.ConnectionError, requests.exceptions.RequestException) as e:
            if trycount==MAXTRYS:    
                print "POST FILE Failed: %s (URL=%s) - EXITING" % (e,url)
                exit(1)
            else:    
                print "POST FILE Failed: %s (URL=%s) - TRYING AGAIN..." % (e,url)
        if r.ok:
            break           
    if not r.ok:    
        print "HTTP FILE POST RESPONSE CODE = %s" % r.status_code
        print str(r.content)
    return r
    
def postResponse(targetD,url,data,raise_for_status=True):
    tov = TIMEOUT
    for trycount in range(1,MAXTRYS):
        try:
            r = requests.post(targetD.urlRoot + url,data=data, params=xmlFormat, headers=targetD.header, timeout=tov, verify=False )
            # If we don't get an OK; code: requests.codes.ok
            if (raise_for_status):
                r.raise_for_status()
        # Check if the REST Request fails
        except (requests.Timeout) as e:
            if trycount==MAXTRYS:    
                print "Timed out while attempting to POST - (URL=%s) - EXITING:" % (url)
                print "    " + str( e )
                exit(1)
            else:    
                tov += 300
                print "Timed out while attempting to POST - (URL=%s) - TRYING AGAIN:" % (url)
            if not targetD.insecure:
                print "Note that insecure connections are only allowed locally"
        # Check if the REST Request fails
        except (requests.ConnectionError, requests.exceptions.RequestException) as e:
            if trycount==MAXTRYS:    
                print "POST Failed: %s (URL=%s) - EXITING" % (e,url)
                exit(1)
            else:    
                print "POST Failed: %s (URL=%s) - TRYING AGAIN..." % (e,url)
        if r.ok:
            break           
    if not r.ok:    
        print "HTTP POST RESPONSE CODE = %s" % r.status_code
        print str(r.content)
    return r
    
def putResponse(targetD,url,data,raise_for_status=True):
    tov = TIMEOUT
    for trycount in range(1,MAXTRYS):
        try:
            r = requests.put(targetD.urlRoot + url,data=data, params=xmlFormat, headers=targetD.header, timeout=tov, verify=False )
            # If we don't get an OK; code: requests.codes.ok
            if (raise_for_status):
                r.raise_for_status()
        # Check if the REST Request fails
        except (requests.Timeout) as e:
            if trycount==MAXTRYS:    
                print "Timed out while attempting to PUT - (URL=%s) - EXITING:" % (url)
                print "    " + str( e )
                exit(1)
            else:    
                tov += 300
                print "Timed out while attempting to PUT - (URL=%s) - TRYING AGAIN:" % (url)
            if not targetD.insecure:
                print "Note that insecure connections are only allowed locally"
        # Check if the REST Request fails
        except (requests.ConnectionError, requests.exceptions.RequestException) as e:
            if trycount==MAXTRYS:    
                print "PUT Failed: %s (URL=%s) - EXITING" % (e,url)
                exit(1)
            else:    
                print "PUT Failed: %s (URL=%s) - TRYING AGAIN..." % (e,url)
        if r.ok:
            break           
    if not r.ok:    
        print "HTTP PUT RESPONSE CODE = %s" % r.status_code
        print str(r.content)
    return r

def getResponse(targetD,url,raise_for_status=True,_prefetch=True):
    tov = TIMEOUT
    for trycount in range(1,MAXTRYS):
        try:
            print "getURL = %s" % targetD.urlRoot + url
            r = requests.get(targetD.urlRoot + url, params=xmlFormat, headers=targetD.header, timeout=tov, verify=False )
            # If we don't get an OK; code: requests.codes.ok
            if (raise_for_status):
                r.raise_for_status()
        # Check if the REST Request fails
        except (requests.Timeout) as e:
            if trycount==MAXTRYS:    
                print "Timed out while attempting to GET - (URL=%s) - EXITING:" % (url)
                print "    " + str( e )
                exit(1)
            else:    
                tov += 300
                print "Timed out while attempting to GET - (URL=%s) - TRYING AGAIN:" % (url)
            if not targetD.insecure:
                print "Note that insecure connections are only allowed locally"
        # Check if the REST Request fails
        except (requests.ConnectionError, requests.exceptions.RequestException) as e:
            if trycount==MAXTRYS:    
                print "GET Failed: %s (URL=%s) - EXITING" % (e,url)
                exit(1)
            else:    
                print "GET Failed: %s (URL=%s) - TRYING AGAIN..." % (e,url)
        if r.ok:
            break           
    if not r.ok:    
        print "HTTP GET RESPONSE CODE = %s (URL=%s)" % (r.status_code,url)
        if (not r.status_code == 404):
            print str(r.content)
    return r
    
def deleteResponse(targetD,url,raise_for_status=True):
    tov = TIMEOUT
    for trycount in range(1,MAXTRYS):
        try:
            r = requests.delete(targetD.urlRoot + url, params=xmlFormat, headers=targetD.header, timeout=tov, verify=False )
            # If we don't get an OK; code: requests.codes.ok
            if (raise_for_status):
                r.raise_for_status()
        # Check if the REST Request fails
        except (requests.Timeout) as e:
            if trycount==MAXTRYS:    
                print "Timed out while attempting to DELETE - (URL=%s) - EXITING:" % (url)
                print "    " + str( e )
                exit(1)
            else:    
                tov += 300
                print "Timed out while attempting to DELETE - (URL=%s) - TRYING AGAIN:" % (url)
            if not targetD.insecure:
                print "Note that insecure connections are only allowed locally"
        # Check if the REST Request fails
        except (requests.ConnectionError, requests.exceptions.RequestException) as e:
            if trycount==MAXTRYS:    
                print "DELETE Failed: %s (URL=%s) - EXITING" % (e,url)
                exit(1)
            else:    
                print "DELETE Failed: %s (URL=%s) - TRYING AGAIN..." % (e,url)
        if r.ok:
            break           
    if not r.ok:    
        print "HTTP DELETE RESPONSE CODE = %s" % r.status_code
        print str(r.content)
    return r
    
def getResponseJSONObj(response):
    if response.ok:
        jsonobj = json.loads(response.content)
        return jsonobj;
        
def getResponseJSONResultSet(response):
    if response.ok:
        return getResponseJSONObj(response).get('ResultSet')
        
def getResponseJSONResults(response):
    if response.ok:
        return getResponseJSONResultSet(response).get('Result')

def getXMLMiniDom(targetD,url):
    # Make a rest request to get the complete XNAT Session XM
    r = getResponse(targetD,url)
    # Parse the XML result into an Element Tree
    return minidom.parseString(r.text.encode(r.encoding))

def getXMLElementTree(targetD,url):
    # Make a rest request to get the complete XNAT Session XML
    r = getResponse(targetD,url)
    # Parse the XML result into an Element Tree
    return etree.fromstring(r.text.encode(r.encoding))

def getXMLStr(targetD,url):
    # Make a rest request to get the complete XNAT Session XML
    r = getResponse(targetD,url)
    # Parse the XML result into an Element Tree
    return r.text.encode(r.encoding)

def getSubjectXMLElementTree(targetD,subject):
    url = "/data/projects/%s/subjects/%s" % (targetD.proj,subject)
    return getXMLElementTree(targetD,url)


def getSessionLabel(subject):    
    return subject + idet.mrLabelExt
        
##########################   
##########################   
##########################   
## MAIN PROGRAM METHODS ##
##########################   
##########################   
##########################   

def convertToSubjectLabel(subject):
    return getSubjectXMLElementTree(idet.target,subject).get('label');

def containsSeries(tree,desc):
    for scanEle in tree.iterfind('.//xnat:scan',NSMAP):
        seriesDesc = scanEle.find('.//xnat:series_description',NSMAP).text
        if seriesDesc.lower() == desc.lower():
            return scanEle
    return None    

def getAssociatedScanIdsFromList(tree,scans,descs,ele):
    returnList = []
    for scanEle in tree.iterfind('.//xnat:scan',NSMAP):
        ckVal = scans[0].find(ele,NSMAP).text
        if ckVal:
                seriesDesc = scanEle.find('.//xnat:series_description',NSMAP).text
                if (seriesDesc in descs or scanEle in scans) and scanEle.find(ele,NSMAP) != None and (ckVal == scanEle.find(ele,NSMAP).text):
                    if not scanEle.get('ID') in returnList:
                        returnList.append(scanEle.get('ID'))
    return returnList            


def getAssociatedScanIds(tree,scan,descs,ele):
    if scan.find(ele,NSMAP) == None:
        return
    ckVal = scan.find(ele,NSMAP).text
    returnList = []
    if ckVal:
        for scanEle in tree.iterfind('.//xnat:scan',NSMAP):
            seriesDesc = scanEle.find('.//xnat:series_description',NSMAP).text
            if (seriesDesc in descs or scanEle == scan) and (ckVal == scanEle.find(ele,NSMAP).text):
                returnList.append(scanEle.get('ID'))
    return returnList            

def getDWIScans(tree,desc):
    returnList = []
    for scanEle in tree.iterfind('.//xnat:scan',NSMAP):
        try:
            seriesDesc = scanEle.find('.//xnat:series_description',NSMAP).text
            if str(seriesDesc.lower()).find("dwi_")==0 and not ("_sbref" in str(seriesDesc.lower()) or "_old" in str(seriesDesc.lower())):
                #print "seriesDesc=" + seriesDesc + " -- ID=" + scanEle.get('ID')
                returnList.append(scanEle)
        except:
            pass
    return returnList
        
def fixBvalFlipCases(subject,expLbl,expTree):
    ## DWI Package
    print "Begin processing bvalue flip fix......"
    packageDesc = "Diffusion"
    dwiScans = getDWIScans(expTree,packageDesc)
    if len(dwiScans)==0:
        print "Session contains no DWI scans.  Does not require bvalue flip fix."
        return
    expID =  expTree.get('ID')
    for scanEle in dwiScans:
        seriesDesc       = scanEle.find('.//xnat:series_description',NSMAP).text
        hasMatch = False
        lrNii = None;
        lrBval = None;
        rlBval = None;
        lrBvec = None;
        rlBvec = None;
        if "_LR_" in seriesDesc:
            print "seriesDesc=" + seriesDesc + " -- ID=" + scanEle.get('ID')
            #print "\n\n\n\n\n"
            scanID = scanEle.get('ID')
            flUrl = "/data/projects/%s/subjects/%s/experiments/%s/scans/%s/resources/NIFTI/files?format=json&locator=absolutePath" % (idet.target.proj,subject,expLbl,scanID)
            flJsonR = getResponseJSONResults(getResponse(idet.target,flUrl));
            for flResult in flJsonR:
                flPath = flResult.get('absolutePath')
                #print "flPath=" + flPath
                if flPath.endswith(".bvec"):
                    lrBvec=flPath
                elif flPath.endswith(".bval"):
                    lrBval=flPath
                elif ".nii" in flPath:
                    lrNii=flPath
            #print "lrNii=" + str(lrNii)
            #print "lrBval=" + str(lrBval)
            #print "lrBvec=" + str(lrBvec)
            for compEle in dwiScans:
                compDesc = compEle.find('.//xnat:series_description',NSMAP).text
                if "_RL_" in compDesc and re.sub("_RL_","_LR_",compDesc)==seriesDesc:
                    hasMatch = True
                    compID = compEle.get('ID')
                    flcUrl = "/data/projects/%s/subjects/%s/experiments/%s/scans/%s/resources/NIFTI/files?format=json&locator=absolutePath" % (idet.target.proj,subject,expLbl,compID)
                    flcJsonR = getResponseJSONResults(getResponse(idet.target,flcUrl));
                    for flcResult in flcJsonR:
                        flcPath = flcResult.get('absolutePath')
                        #print "flcPath=" + flcPath
                        if flcPath.endswith(".bvec"):
                            rlBvec=flcPath
                        elif flcPath.endswith(".bval"):
                            rlBval=flcPath
                #print "rlBval=" + str(rlBval)
                #print "rlBvec=" + str(rlBvec)
            if not hasMatch:
                print "ERROR:  Could not find matching scan for bval fix - " + seriesDesc
                exit(1)
            else:
                if lrNii==None or lrBval==None or lrBvec==None or rlBval==None or rlBvec==None:
                    print "ERROR:  Could not find all necessary bvec/bval files"
                    exit(1)

            #print "lrNii=" + lrNii
            #print "lrBval=" + lrBval
            #print "lrBvec=" + lrBvec
            #print "rlBval=" + rlBval
            #print "rlBvec=" + rlBvec

            procCall = "%s/includes/processFlip.sh --subject %s --lrNii %s --lrBval %s --lrBvec %s --rlBval %s --rlBvec %s" % (os.path.dirname(__file__),subject,lrNii,lrBval,lrBvec,rlBval,rlBvec)
            print "%s/includes/processFlip.sh --subject %s --lrNii %s --lrBval %s --lrBvec %s --rlBval %s --rlBvec %s" % (os.path.dirname(__file__),subject,lrNii,lrBval,lrBvec,rlBval,rlBvec)
            rc = os.system(procCall)

            ## Update Stats, Checksums
            statUrl = "/data/services/refresh/catalog?options=populateStats,checksum&resource=/archive/experiments/%s/scans/%s/resources/NIFTI" % (expID,scanID)
            rc=postResponse(idet.target,statUrl,None)
			
    exit(0)
    
#############################   
#############################   
#############################   
## MAIN PROCESSING SECTION ##
#############################   
#############################   
#############################   
        
idet = getReleaseAndConnDetailsFromInput()        
    
global subject

for expLbl in idet.expList:

    print("\nEXPERIMENT=" + expLbl + " - Begin Processing (fixBvalFlipCases) ....\n")

    subject = expLbl.split("_")[0]
    expUrl = "/data/projects/%s/subjects/%s/experiments/%s?format=xml&concealHiddenFields=true" % (idet.target.proj,subject,expLbl)
    expTree = getXMLElementTree(idet.target,expUrl)

    if subject != "199251" and subject != "585862" and subject != "896778":
        print "Subject %s does not require bvalue flip fix.  Skipping....." % subject
        continue
    
    #print str(expTree)
    fixBvalFlipCases(subject,expLbl,expTree)
    print("\nSUBJECT=" + subject + " - Finished Processing (fixBvalFlipCases) ....\n")
    
###

