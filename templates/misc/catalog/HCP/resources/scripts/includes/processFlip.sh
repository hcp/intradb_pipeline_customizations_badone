#!/bin/bash

## SET UP FSL
export FSLDIR=/nrgpackages/tools.release/fsl-4.1.9-nrg
source /nrgpackages/tools.release/fsl-4.1.9-nrg/etc/fslconf/fsl.sh
export PATH=${FSLDIR}/bin:${PATH}

SUBJECT=""
LR_NII="_NS_"
LR_BVAL="_NS_"
LR_BVEC="_NS_"
RL_BVAL="_NS_"
RL_BVEC="_NS_"

while true; do 
    case "$1" in
      --help | -h | -\?)
	echo "processFlip.sh [options]"
	echo "   Options"
	echo "      --subject            <subject label>"
	echo "      --lrBval             <Path to LR scan BVAL file>"
	echo "      --lrBvec             <Path to LR scan BVEC file>"
	echo "      --rlBval             <Path to RL scan BVAL file>"
	echo "      --rlBvec             <Path to RL scan BVEC file>"
	exit 0
	;;
      --subject)
        SUBJECT=$2
	shift
	shift
        ;;
      --lrNii | -U)
        LR_NII=$2
	shift
	shift
        ;;
      --lrBval | -U)
        LR_BVAL=$2
	shift
	shift
        ;;
      --lrBvec | -U)
        LR_BVEC=$2
	shift
	shift
        ;;
      --rlBval | -U)
        RL_BVAL=$2
	shift
	shift
        ;;
      --rlBvec | -U)
        RL_BVEC=$2
	shift
	shift
        ;;
      -*)
	echo "Invalid parameter ($1)"
	exit 1
        ;;
      *)
	break 
        ;;
    esac
done

SCRIPTDIR=`dirname $0`
SCRIPTDIR=`readlink -f $SCRIPTDIR`

if [ "$SUBJECT" = "_NS_" ]; then
	echo "SUBJECT NOT SPECIFIED"
	exit 1
fi
if [ "$LR_NII" = "_NS_" -o ! -w "$LR_BVAL" ]; then
	echo "LR_NII file either does not exist, is not specified or is not writable [$LR_NII]"
	exit 1
fi
if [ "$LR_BVAL" = "_NS_" -o ! -w "$LR_BVAL" ]; then
	echo "LR_BVAL file either does not exist, is not specified or is not writable [$LR_BVAL]"
	exit 1
fi
if [ "$LR_BVEC" = "_NS_" -o ! -w "$LR_BVEC" ]; then
	echo "LR_BVEC file either does not exist, is not specified or is not writable [$LR_BVEC]"
	exit 1
fi
if [ "$RL_BVAL" = "_NS_" -o ! -w "$RL_BVAL" ]; then
	echo "RL_BVAL file either does not exist, is not specified or is not writable [$RL_BVAL]"
	exit 1
fi
if [ "$RL_BVEC" = "_NS_" -o ! -w "$RL_BVEC" ]; then
	echo "RL_BVEC file either does not exist, is not specified or is not writable [$RL_BVEC]"
	exit 1
fi

#echo "LR_BVAL=$LR_BVAL"
#echo "LR_BVEC=$LR_BVEC"
#echo "RL_BVAL=$RL_BVAL"
#echo "RL_BVEC=$RL_BVEC"

if [ -f "${LR_BVAL}.orig" -a ! -f ${LR_BVAL}.orig.bak ] ; then
	cp ${LR_BVAL}.orig ${LR_BVAL}.orig.bak 
fi
mv ${LR_BVAL} ${LR_BVAL}.orig 
if [ -f "${LR_BVEC}.orig" -a ! -f ${LR_BVEC}.orig.bak ] ; then
	cp ${LR_BVEC}.orig ${LR_BVEC}.orig.bak 
fi
mv ${LR_BVEC} ${LR_BVEC}.orig 

nframesLR=`fslnvols $LR_NII`

cut $RL_BVAL -d " " -f 1-${nframesLR} > $LR_BVAL
cut $RL_BVEC -d " " -f 1-${nframesLR} > $LR_BVEC

## Remove digest values from catalog file so they can be recomputed
LR_DIR=`dirname $LR_BVEC`
if [ -d "$LR_DIR" ] ; then
	sed -i "s/ digest=[^ ]* / /g" $LR_DIR/*catalog.xml
fi

